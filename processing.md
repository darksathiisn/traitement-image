# ![duez](images/pythonImage.png) **Traitements de l'image en Python via [PROCESSING](https://processing.org/)**

## ![titre](images/titre.png) **Les consignes**

+ Copier dans votre dossier **SNT** de votre espace de travail le dossier processing.
+  Vous placerez l’intégralité de vos réalisations de ce chapitre dans votre dossier **H:/travail/SNT/processing**. Toutes réalisations enregistrées dans un espace de stockage différent ne seront pas prises en compte.
+  Une page HTML devra être créée pour répondre aux questions marquées par le logo ![item](images/trooperLogo.png). On prendra soin de créer des liens vers les images et les fichiers qui auront été l’objet de modifications liées aux questions posées. 

## ![titre](images/titre.png) **Introduction**


Le célèbre format bitmap, qui tire son nom de l'anglais "bitmap" pour carte de bits montre qu'une image est avant tout un domaine spatial sur lequel on peut se promener avec la souris de l'ordinateur : les distances en pixels dans l'image I sont dËs lors liées aux distances rÈelles en mètres dans la scène
réelle S. 

La fréquence spatiale est un concept délicat qui découle du fait que les images appartiennent au domaine spatial. Pour commencer on peut rappeler que la fréquence est une grandeur qui caractérise le nombre de phénomènes qui se déroulent au cours d'un temps donné : en voiture le long d'une route vous voyez 2 bandes blanches PAR seconde : c'est une fréquence temporelle. Il est ensuite facile de comprendre que ce concept de fréquence "temporelle" peut aussi se traduire en disant qu'il y a 200 bandes blanches par kilomètre : c'est une fréquence spatiale.

Dans une image les détails se répètent fréquemment sur un petit nombre de pixels, on dit qu'ils ont une fréquence élevée : c'est le cas pour les bords et les contours dans une image.
Au contraire, les fréquences basses correspondent à des variations qui se répètent peu car, diluées sur de grandes parties de l'image, par exemple des variations de fond de ciel.
Nous verrons dans la suite que la plupart des filtres agissent sÈlectivement
sur ces fréquences pour les sélectionner, en vue de les amplifier ou de les réduire.



## ![titre](images/titre.png) **Réalisation de filtres** 

**On utilisera dans cette partie le logiciel processing : [PROCESSING](https://processing.org/)**


![item](images/trooperLogo.png) **Afficher une image : Testez ce code**

![afficher](images/afficherImageProcessing.png)

![item](images/trooperLogo.png) **Manipuler les pixels : Testez ce code**

![afficher](images/pixelProcessing.png)

![item](images/trooperLogo.png) **Dupliquer une image : Testez ce code**

![afficher](images/yoda25.PNG)

![item](images/trooperLogo.png) **Coder pour obtenir cette image** (l'image de yoda est [ici](images/yoda.png))

![afficher](images/yoda100.PNG)

![item](images/trooperLogo.png) **Codage d'un filtre rouge : Testez ce code**

![afficher](images/filtreRouge.PNG)

![item](images/trooperLogo.png) **Réaliser le codage d'un filtre vert**

![item](images/trooperLogo.png) **Réaliser le codage d'un filtre bleu**

![item](images/trooperLogo.png) **Tester vos filtres sur les images de votre choix**

## ![titre](images/titre.png) **Modifier une image par convolution : Filtre de convolution**

![item](images/trooperLogo.png) **C'est quoi un filtre de convolution ?**

Beaucoup de traitements d'images sont basés sur les produits de convolutions, dont nous allons expliquer le principe.

L'image numérique étant en quelque sorte une carte de pixels, on peut identifier chaque pixel par ses coordonnées x et y et lui affecter une valeur liée sa luminosité. On peut utiliser dans le cadre des images numériques une sorte un tableau de x colonnes et y lignes qui réserve une place pour ranger la valeur de chaque pixel de l'image. En mathématique ce genre de
tableau s'appelle une matrice, et les mathématiciens disposent d'outils pour effectuer des calculs sur les matrices, comme additionner deux matrices, les multiplier, etc ...

Un produit de convolution, est un opérateur mathématique qu'on utilise pour multiplier des matrices entre elles.

Dans le cas qui nous intéresse, nous mettons en jeu deux matrices trés différentes: la matrice image, trés grande (par exemple 512 x 512, ce qui représente 262144 pixels ) et une matrice plus petite qu'on appelle le noyau parce que c'est le "coeur" de tous les changements qui vont affecter l'image.

Le noyau va donc agir sur chacun des pixels, c'est à dire sur chacun des éléments de la matrice "image".

![item](images/trooperLogo.png) **La détection des contours à l'aide du noyau de Sobel : Testez ce code**

![sobel](images/detectionContours.PNG)

![item](images/trooperLogo.png) **Tester ce code sur un auto-portrait.**

![item](images/trooperLogo.png) **Chercher un autre noyau de convolution et tester le.**

![item](images/trooperLogo.png) **Le filtre de convolution passe haut.**

Un filtre passe haut favorise les hautes fréquences spatiales, comme les détails, et de ce fait, il améliore le contraste. Un filtre passe haut  est caractérisé par un noyau comportant
des valeurs négatives autour du pixel central.

Modifier le programme contour (Que l'on renommera passeHaut) pour en faire un filtre passe haut à l'aide du noyau ci-dessous.


```python
passeHaut = [[-1, -1, -1],
            [-1, 9, -1],
            [-1, -1, -1]]
```

![item](images/trooperLogo.png) **Appliquer le filtre à l'image ci-dessous :**

[NGC 7293 – NÉBULEUSE HELIX](images/HelixHa.jpg)

![item](images/trooperLogo.png) **Justifier l'utilité de ce filtre sur des clichés d'objets astronomiques.**

![item](images/trooperLogo.png) **Faire des recherches sur les filtres de convolution passe bas. Et tester un noyau de cette classe de filtre sur une image bien choisie.**

![item](images/trooperLogo.png) **Dans quelle catégorie se situe chacun des filtres suivants**



```python
# Noyau Prewitt horizontal
PrewittH = [[-1, 0, 1],
             [-1, 0, 1],
             [-1, 0, 1]]
```


```python
# Noyau Binomial gaussien
binoGauss = [[1/16, 1/8, 1/16],
             [1/8, 1/4, 1/8],
             [1/8, 1/8, 1/8]]
```


```python
# Noyau de Kirsh
kirsh = [[5, 5, 5],
             [-3, 0, -3],
             [-3, -3, -3]]
```


```python
# Noyau de Robinson
robinson = [[1, 1, 1],
             [1, -2, 1],
             [-1, -1, -1]]
```

```python
# Noyau dit repoussage
repoussage = [[-2, -1, 0],
             [-1, 1, 1],
             [0, 1, 2]]
```

##  ![projet](images/projet.png) Serez-vous capable de programmer ces transformations ?

![item](images/trooperLogo.png) **Pour cette transfornation, compléter simplement le code.**

![sithSymH](images/sithSymH.PNG)


```python
def settings():    
    
    global picture
    picture = loadImage(____________) 
    size(________, _________)
   
        
def setup():
    
    global canvas            
    canvas = createImage(picture.width, picture.height, RGB)    
    for x in range(picture.width):
        for y in range(picture.height):
            indice = ____________________
            indiceSym = x + (picture.height - y - 1)*picture.width
            rouge = red(picture.pixels[indice])
            vert = green(_________________)
            bleu = ________________________
            canvas.pixels[indiceSym] = color(rouge, _____, _______)

def draw():     
    image(canvas, 0, 0)
    image(picture, 0, 0, picture.width*0.30, picture.height*0.30)
```

![item](images/trooperLogo.png) **À vous de jouer! Il faudra vous inspirer sans retenu du précédent code.**


![sithSymV](images/sithSymV.PNG)
