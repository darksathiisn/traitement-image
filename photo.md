# ![num](images/photonum2.png) **La photographie numérique**

## ![titre](images/titre.png) **Les consignes**

+ Copier dans votre dossier **SNT** de votre espace de travail le dossier photographie.
+  Vous placerez l’intégralité de vos réalisations de ce chapitre dans votre dossier **H:/travail/SNT/photographie**. Toutes réalisations enregistrées dans un espace de stockage différent ne seront pas prises en compte.
+  Une page HTML devra être créée pour répondre aux questions marquées par le logo ![item](images/trooperLogo.png). On prendra soin de créer des liens vers les images et les fichiers qui auront été l’objet de modifications liées aux questions posées. 

## ![titre](images/titre.png) **Le binaire**

La compréhension de certaines fonctionnalités des appareils photo numérique(APN) et des logiciels de traitement d’image nécessite une connaissance basique du calcul binaire et de la manière dont les données sont représentées en technologie numérique.

Dans tout dispositif numérique (ordinateur, APN, téléphone portable,…), l’élément le plus petit qui sert à représenter une information est une position mémoire qui ne peut
prendre que 2 valeurs : 0 ou 1. C’est la base de tout le système dit binaire (ainsi nommé puisque l’élément de base ne peut se trouver que dans 2 états différents). 

Quand il a écrit « Il faut qu’une porte soit ouverte ou fermée », Alfred de Musset a jeté les bases du système binaire.

Cette « position binaire » s’appelle un bit. C’est l’abrégé de « binary digit ».

Pour des raisons pratiques, les programmeurs (et les ordinateurs) n’utilisent pas directement le bit comme base de travail mais un « paquet » de 8 bits que l’on appelle
l’octet (ou le byte en anglais). Cela est dû au fait que les programmeurs ne calculent pas avec le système décimal que nous connaissons tous (basé sur 10 chiffres de 0 à 9)
mais avec le système hexadécimal (basé sur 16 chiffres de 0 à F : 0,1,…,8, 9, A, B, C, D, E, F – A = 10, B = 11, … F = 15).

Une valeur quelconque sera donc représentée au minimum par un octet (ou par un multiple d’octets). Le bit est très limité puisqu’il ne peut représenter que les valeurs 0 et 1. 

Si on utilise un paquet de 8 bits, les possibilités sont déjà plus grandes : chacun des 8 bits de l’octet pouvant prendre la valeur 0 ou 1, il y a en fait 256 combinaisons
possibles (de 0 à 255). 

![item](images/trooperLogo.png) **Que signifie l'acronyme ```BIT```?**

## ![titre](images/titre.png) **Un peu d'histoire ...(Harold Phelippeau)**

L’homme perçoit majoritairement le monde extérieur avec son système de vision. Il a depuis toujours essayé de reproduire et de figer ce qu’il voit. Longtemps, le dessin et la
peinture sont restés les seuls moyens de représentation du monde. L’ancêtre de l’appareil photo, la chambre noire, qui permet de projeter sur une surface plane l’image d’une scène
était déjà connue à l’époque d’Aristote (384-322 av. J.-C.). 

Ce principe, couplé à des systèmes optiques, est utilisé par certains artistes à l’époque de la renaissance pour faciliter le tracé des objets en perspective. Le dispositif physique permettant de projeter l’image d’une scène sur un support étant connu, il reste alors une étape importante à franchir : comment faire en sorte que cette image s’imprime sur le support de façon stable et durable ? En 1826, J.N. Niépce (1765-1833), réussit à obtenir et à conserver une image fixe et durable gràce à l’utilisation du chlorure d’argent. 

En 1833, l’utilisation de plaques de cuivre recouvertes de iodure d’argent exposées ensuite à des vapeurs de mercure améliore le procédé de fixation et diminue de manière importante le temps d’exposition. L’invention du négatif par W.H.Fox-Talbot(1800-1877) permet de reproduire plusieurs images à partir d’une seule exposition. Vers la fin des années 1880, l’utilisation de la photographie est limitée par son coût et sa complexité. Toutefois, quand en 1888 George Eastman (1854-1932) lance le Kodak, un appareil-photo portatif très maniable et doté d’une pellicule, la voie s’est dégagée pour le photographe amateur. Quand un client avait pris ses photos, il retournait l’appareil entier à l’usine. La pellicule était traitée, l’appareil rechargé, puis réexpédié avec des photos d'eveloppées. Le slogan « Appuyez sur
le bouton, nous ferons le reste » n’avait rien d’exagéré. Les milliards de clichés pris chaque année indiquent que son succès ne s’est jamais démenti. Aujourd’hui, la popularité de la
photographie s’est accrue grâce à l’arrivée des appareil-photos numériques. Ils offrent une grande souplesse et une convivialité d’utilisation. Le contrôle du résultat est immédiat
par prévisualisation sur un écran LCD. Les images sont instantanément disponibles, il est possible de les effacer, de les reproduire et de les échanger. Le nombre de prises de vues
est plus élevé, en effet, l’utilisateur n’a plus la crainte de rater une photographie. Il est aussi possible de corriger et de manipuler ult´erieurement les images sur un ordinateur.

**... Et arrive la photographie numérique**

Jusqu’en 1981, il était nécessaire d’utiliser des systèmes d’appareil-photos analogiques pour convertir un signal lumineux en un signal électrique. En 1981, Sony introduit le
système Mavica, capable d’enregistrer les images sur une disquette magnétique. Ainsi commença l’histoire de la photographie numérique grand public. En 1988, Fuji Photo
Film annonce le premier appareil-photo numérique, le Fujix DS-1P, celui-ci utilise une carte mémoire pour enregistrer les images. A l’exposition Photokina de 1990,
Fuji Photo Film et Olympus exposent le prototype des appareil-photos numériques d’aujourd’hui. Il est composé d’un ASIC (Application Specific Integreted Circuit) qui
permet une compression algorithmique standardisée en 1992 par le JPEG (Joint Photographic Expert Group). 

Apple introduit en 1994 le QuickTake 100 comme appareil-photo pour ordinateur. En 1995, le QV-10 de Casio connaît un succès commercial et ouvre une nouvelle ère dans la consommation et l’utilisation des appareil photos numériques. A la Photokina de 1996, Olympus présente le XGA C-800L, cet appareil est capable de traiter des images de 810 000 pixels. Le XGA améliore ainsi la qualité des images obtenues numériquement, en se rapprochant de la qualité des appareils argentiques. En 1997, Olympus présente le C-1400, c’est le premier appareil destiné
au marché grand public qui possède une dalle CCD contenant plus de 1 million de pixels.

A partir de 1999, les capteurs de plus de 1 million de pixels se généralisent. En 2003, Canon présente le premier réflexe numérique grand publique, l’EOS 300D aussi appelé Rebel. A partir de 2004, les capteurs d’images numériques font leur apparition dans les téléphones mobiles, principalement avec une résolution VGA (640 × 480). Suivant une évolution croissante, on trouve aujourd’hui des appareil-photos réflex numériques possédant des dalles photosensibles allant jusqu’à 17 millions de pixels et des téléphones portables intégrants des capteurs de 12 millions de pixels.

### ![icone](images/iconephoto.png) **Comment voyons-nous le monde qui nous entoure ?**

![item](images/trooperLogo.png) **Comment s'appelle la technique qui permet de projeter sur une surface plane l’image d’une scène**




![vision](images/Champ_vision.png)

+ Est-ce que nous voyons réellement ce qui nous entoure ?
+ La rétine envoie-t-elle l'image au cerveau ?

Notre cerveau reconstitue une image virtuelle. La rétine envoie trés peu d'information au cerveau.

L'oeil voit essentiellement les objets en mouvement. L'immobile n'arrive pas au cerveau.

Pour reconstituer une image, l'oeil balaye constamment la scène en adaptant en permanence la distance et la lumière.

> **Rétine** : Système de traitement du signal.
>
> **Cortex Visuel** : Système algorithmique de réalité virtuelle.

**L'image que nous croyons voir n'est qu'une reconstruction algorithmique.**

![item](images/trooperLogo.png) **Donner une description précise de ce que vos yeux voient sur cette image.**

![spirale](images/spirale.png)

### ![icone](images/iconephoto.png) **La photographie numérique ou le trucage de la réalité !**

**La photographie numérique est un trucage de la réalité pour mieux la mettre en valeur.**

On fait croire au cerveau qu'il voit une vraie scène, même s'il ne regarde pas en réalité ure scène. C'est un "trucage cyber-physique", c'est à dire un mélange d'informatique et de sciences physiques.

Quels sont ces trucages :
- reconstruction de l’image du capteur
- réduction du bruit
- équilibre des couleurs
- réglage de la luminosité et du gamma
- correction des aberrations géométriques
- redimensionnement des images (downsampling)
- compression
- etc ...

### ![icone](images/iconephoto.png) **La capture de la lumière : Le capteur d'un appareil photo numérique.**

Le capteur est composé de **photosites** qui transforment l’énergie lumineuse (les photons) en énergie électrique, à ne pas confondre avec les pixels, terme que l’on utilise uniquement en diffusion, après la captation. *Généralement*, le pixel est composé de 4 photosites (un rouge, un bleu et deux verts).

**Capture de la lumière**.

![capture de la lumière](images/317944.jpg)

- **a** : Microlentilles(On diriger le spectre lumineux et optimiser la surface) | filtre bloquant les infrarouges.
- **b** : Matrice de Bayer(Matrice principalement utilisée).
- **c** : Photosites.
- **d** : Traitement analogique et digital du signal.(Valeur sur 12 bits ou 14 bits que l'on appelle la **luminance**.


**Le capteur CMOS du Nikon D2H grossi 10 fois**.

![capteur](images/cmosmicrograph-2.jpg)

**Le capteur CMOS du Nikon D2H grossi 40 fois, on peut voir distinctement les photosites**.

![capteur](images/cmosmicrograph-3.jpg)


Un jeu de filtre couleur (R,V,B) permet de « spécialiser » chaque photosites en une couleur primaire particulière. La mosaïque la plus connue pour nos boitiers est la matrice de Bayer. Basée sur une structure qui se répète, chaque photosites va être sensible au spectre lumineux (comprenez lumière) qui va venir le frapper. A l’origine, le photosite est sensible à l’intensité et non à la couleur, voilà comment nous obtenons une image non noir et blanc. On ajoute également des micro-lentilles sur chaque photosites pour diriger le spectre lumineux et optimiser la surface.



![item](images/trooperLogo.png) **Matrice de Bayer : Observé cette matrice. Que remarquez-vous? Donner une explication.**

**Matrice de Bayer**.

![capteur](images/bayer.jpg)

![item](images/trooperLogo.png) **Rechercher d'autre matrice.**

### ![icone](images/iconephoto.png) **La luminance d'un photosite**

Si vous consultez la documentation technique de votre APN, vous trouverez certainement une information sur la « profondeur en bits » utilisée par le capteur pour
représenter chaque pixel : en général 12-bit ou 14-bit. Le photosite qui capture un point de l’image sur le capteur ne peut pas encore être considéré comme un pixel. La couleur
d’un pixel est représentée par 3 valeurs (R, V et B) mais à la prise de vue, chaque point de l’image n’est représenté que par une seule valeur : sa luminance.

Le capteur ne sait pas capturer pour un point donné les 3 valeurs Rouge, Vert et Bleu qui représentent ce point. Il ne capture qu’une seule couleur. Au moment de l’illumination, le capteur enregistre une donnée analogique (le courant généré par les photons arrivant sur le photosite) et transforme cette valeur analogique (une intensité de courant) en valeur binaire par le biais d’un convertisseur analogique/digital (ADC – Analog Digital Converter). La valeur binaire résultante est, selon le capteur, stockée sur 12-bit ou 14-bit (en général). 

Mais cette valeur ne représente qu’une seule couleur. C’est le processus de dématriçage (qui a toujours lieu que vous utilisiez le format JPEG ou le **format RAW** – dans le premier cas ça se passe dans votre APN et dans le second dans votre logiciel) qui va au final affecter, **par interpolation**, un ensemble de 3 valeurs (rouge, vert et bleu) à chaque photosite.

Selon le logiciel utilisé et les options que vous aurez choisies, ces composantes RVB seront chacune codées sur 8 bits ou sur 16 bits.

**Le format RAW**

un fichier RAW constitue le négatif numérique de votre image. C’est une photo non développée et à ce stade, la décision de la sortir en couleur ou en N&B n’a même pas encore été prise.

Ce négatif doit donc être développé. C’est le rôle du **dématriceur**.

Le dématriceur vous permet de fixer les paramètres de développement sans affecter le négatif lui même. On obtiendra un fichier au format JPEG après le dématriçage.

Le fichier RAW contient toutes les informations enregistrées par le capteur. La quantité d’informations à partir de laquelle vous allez travailler dans la phase de retouche est donc beaucoup plus grande et les corrections potentielles beaucoup plus fines et plus étendues. Et donc au final, vous bénéficierez d’une meilleure qualité d’image.

Un fichier RAW contient les données brutes de la prise de vue ainsi qu'une une vignette JPEG utilisée pour la visualisation avant développement.

Puisque toutes les données enregistrées par le capteur sont présentes dans le fichier RAW, la conséquence immédiate est que la photo va prendre beaucoup plus de place sur votre
carte mémoire et réduire ainsi sa capacité en nombre de photos enregistrées.

### ![icone](images/iconephoto.png) **Le dématriçage**

Les capteurs ne sont pas naturellement sensibles à l’information de couleurs, ils sont seulement capables de mesurer la quantité de lumière (le nombre de photons) percutant chaque photosite. Pour introduire la sensibilité chromatique, un filtre coloré est superposé sur la surface des capteurs. Ces filtres sont compos´es d’une mosaïque des trois composantes de couleurs primaires rouge, vert et bleu. La combinaison de ces trois composantes par synthèse additive permet de reconstruire une partie de l’ensemble des couleurs dans le diagramme CIE 1931 xy du spectre visible.

### **Définition** ##

Le dématriçage (aussi connu sous le néologisme débayerisator) est une des phases du traitement du signal brut issu du capteur d'un appareil photographique numérique. Il consiste à **interpoler** les données de chacun des photosites monochromes rouge, vert et bleu composant le capteur électronique pour obtenir une valeur trichrome pour chaque pixel.

La mosaïque de Bayer, comportant une moitié de capteurs à filtre vert disposés en quinconce, séparés par des capteurs à filtre alternativement rouge et bleu, est la disposition la plus courante pour les capteurs couleur. Le dématriçage est l'opération de base du traitement de l'image ; il s'accompagne souvent d'opérations correctives. Il se fait dans l'appareil et, si on a enregistré un fichier RAW, avec un logiciel spécialisé, tenant compte des caractéristiques de l'appareil.

https://fr.wikipedia.org/wiki/Dématriçage

![dematricage](images/Algorithme_de_dematricage.png)

Comment reconstituer à partir de ces informations une véritable image couleur ? En appliquant un **algorithme de dématriçage** aux informations brutes enregistrées par le
capteur. C’est une opération purement mathématique qui consiste pour chaque point :

- À enregistrer telle quelle l’intensité de lumière correspondant à la couleur du filtre pour ce photosite.
- À examiner la valeur des intensités des 2 autres couleurs dans les 8 sites qui entourent le site en cours d’examen et à en déduire une valeur que l’on affectera au site en cours de traitement.

### ![icone](images/iconephoto.png) **Le dématriçage à l'aide de python : Le module Rawpy**

**Dans la suite nous allons travailler sur un fichier raw d'un portrait de votre "trombine :-)"**

**Une photo sans dématriçage**

![raw](images/rawLuminances.png)  ![raw](images/rawLuminancesDetail.png)

### ![titre](images/titre.png) **Obtenir des informations à partir du fichier raw**


```python
import rawpy
import matplotlib.image as mpimg
import os

os.chdir (input('Donne le répertoire où sont stockés les fichiers raw : '))
imageRaw = input('Avec quel fichier raw veux-tu travailler? :')
```

    Donne le répertoire où sont stocké les fichier raw :  O:\c2j
    Avec quel fichier raw veux-tu travailler? : Raw_partie3_pap1.ARW
    

**Description en chaîne des couleurs numérotées de 0 à 3 (RGBG, RGBE, GMCY ou GBTG).**

> Notez que les mêmes lettres ne peuvent pas se référer strictement à la même couleur. 
>
> Il existe des filtres avec deux verts différents par exemple.


```python
with rawpy.imread(imageRaw) as raw:
    print(raw.color_desc)
```

    b'RGBG'
    

**Obtenir la matrice de l'APN**


```python
with rawpy.imread(imageRaw) as raw:
    print(raw.raw_colors)
```

    [[0 1 0 ... 1 0 1]
     [3 2 3 ... 2 3 2]
     [0 1 0 ... 1 0 1]
     ...
     [3 2 3 ... 2 3 2]
     [0 1 0 ... 1 0 1]
     [3 2 3 ... 2 3 2]]
    

**Obtenir les luminances des photosites**


```python
with rawpy.imread(imageRaw) as raw:
    print(raw.raw_image)
```

    [[605 767 577 ... 647 567 647]
     [771 693 769 ... 577 649 577]
     [586 765 592 ... 634 565 634]
     ...
     [719 658 733 ... 736 809 736]
     [573 719 575 ... 818 620 818]
     [717 641 719 ... 719 833 719]]
    

**Obtenir les luminances d'un photosite**


```python
with rawpy.imread(imageRaw) as raw:
    print(raw.raw_image[1000, 200])
```

    632
    

### ![titre](images/titre.png) **Ton premier dématriçage d'un fichier raw**

**Tu dois copier le code dans un fichier que tu nommeras raw.py à partir du l'EDI Thonny.**


```python
import rawpy
import matplotlib.image as mpimg
import os

os.chdir (input('Donne le répertoire où sont stocké les fichier raw : '))
imageRaw = input('Avec quel fichier raw veux-tu travailler? :')

with rawpy.imread(imageRaw) as raw:
    image = raw.postprocess()
mpimg.imsave("raw.png", image)
```

    Donne le répertoire où sont stocké les fichier raw :  I:\NSI
    Avec quel fichier raw veux-tu travailler? : 3rWHA7Q0Lf.arw
    

![raw](images/raw.png)

-----
**``use_auto_wb (bool)`` : calculer automatiquement la balance des blancs**

![item](images/trooperLogo.png) **Rechercher ce que veut dire calculer la balance des blancs en photographie**


```python
with rawpy.imread(imageRaw) as raw:
    image = raw.postprocess(use_auto_wb = True)
mpimg.imsave("rawblancs.png",image)
```

![raw](images/rawblancs.png)

**`user_wb (liste)` : liste de longueur 4 avec multiplicateurs de balance des blancs pour chaque couleur**

Tu peux aussi faire tes propres réglages de balance des blancs. Teste le réglage suivant, et essaye de trouver une application utile déjà vu!


```python
with rawpy.imread(imageRaw) as raw:
    image = raw.postprocess(user_wb = [4, 0.2, 0.3, 0.5])
mpimg.imsave("rawblancsPerso.png", image)
```

![rawblancsPerso](images/rawblancsPerso.png)

-----
**``use_auto_wb (bool)`` : réglage de la saturation**

![item](images/trooperLogo.png) **Rechercher des informations sur la saturation en photographie**


```python
with rawpy.imread('3rWHA7Q0Lf.arw') as raw:
    pixels = raw.postprocess(user_sat= 1500)
mpimg.imsave("raw.png", pixels)
```

![rawblancsPerso](images/rawSat.png)

-----
**``bright (float)`` : mise à l'échelle de la luminosité**

![item](images/trooperLogo.png) **Rechercher des informations sur la luminosité d'une image**


```python
with rawpy.imread('3rWHA7Q0Lf.arw') as raw:
    pixels = raw.postprocess(bright = 10)
mpimg.imsave("rawBri.png", pixels)
```

![lum](images/rawBri.png)



### ![icone](images/iconephoto.png) **Les métadonnées : A FAIRE EN DEVOIR MAISON**

Comme une image numérique est contenue dans un fichier et non pas observable
directement comme une image argentique, il faut utiliser un logiciel pour l’afficher ou
l’imprimer. 

Ce logiciel doit prendre certaines décisions rapidement et ne peut se permettre d’analyser la totalité des données représentant l’image (une suite d’octets représentant des pixels) à chaque fois que l’image doit être manipulée. 

Il est beaucoup plus pratique d’insérer dans le fichier, à côté des données brutes de représentation de l’image, des informations décrivant cette image, son format, sa définition, les conditions dans lesquelles elle a été prise, etc. 

Ce sont les métadonnées photographiques. Dans ces métadonnées on pourra également trouver une autre image de définition réduite (la vignette) qui permet de prendre rapidement connaissance du contenu de l’image sans avoir à manipuler la totalité des données.

C’est le cas des fichiers RAW par exemple qui contiennent toujours une petite vignette JPEG qui permet de visualiser rapidement l’image sur l’écran de l’APN ou dans une visionneuse. Cette vignette est une métadonnée.

![item](images/trooperLogo.png) **Ouvrir le logiciel DarkTable(Depuis le dossier commun copier sur votre bureau le dossier ```darktablePortable```).**

![item](images/trooperLogo.png) **Donner les dimensions de l'image.**

![item](images/trooperLogo.png) **Avec quel APN a été prise cette photo?**

![item](images/trooperLogo.png) **Le photographe a-t-il déclanché son flash?**

![item](images/trooperLogo.png) **Donner la date de réalisation de cette prise de vue.**

![item](images/trooperLogo.png) **Est-ce qu'il y a eu utilisation d'un zoom?**

![item](images/trooperLogo.png) **Comment prendre des photos au format RAW depuis votre smartphone?**

