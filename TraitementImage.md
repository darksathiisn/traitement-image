# ![duez](images/photo.png) **Le codage des images**

## ![titre](images/titre.png) **Les consignes**

+ Copier dans votre dossier **SNT** de votre espace de travail le dossier codage des images.
+  Vous placerez l’intégralité de vos réalisations de ce chapitre dans votre dossier **H:/travail/SNT/CodageImage**. Toutes réalisations enregistrées dans un espace de stockage différent ne seront pas prises en compte.
+  Une page HTML devra être créée pour répondre aux questions marquées par le logo ![item](images/trooperLogo.png). On prendra soin de créer des liens vers les images et les fichiers qui auront été l’objet de modifications liées aux questions posées. 

## ![titre](images/titre.png) **Image matricielle ou bitmap**

L’image est divisée en (m colonnes ×n lignes) cellules appelées  pixel. On associe alors une couleur à  chaque pixel selon divers encodages. 
La  définition  d'une image numérique correspond au  nombre de points  (pixels) qui la composent. La  résolution  d'une image est définie par  un  nombre de pixels unité de longueur de l’image numérique affichée ou  numérisée c'est-à-dire la densité de pixel de l’image. La résolution s’exprime  en  ppp  (pixels par pouce). L’ensemble des deux définit la taille de l’image. 

**L’image ci-dessous possède une définition de 1024×577 et une résolution de 96 ppp.**

La taille de l’image est donc :

$Largeur =  \dfrac{1024}{96} \approx 10,7 pouces \approx 10,7\times 2,54\approx 27,1cm$

$Longueur=   \dfrac{577}{96}=6,01 pouces=6,01\times 2,54\approx 15,27cm$
            

![sith](images/sith.jpg)

> **Clique bouton droit sur l'image : Menu propriété**

![propriété](images/propriete.png)


![item](images/trooperLogo.png) **Déterminer le nombre de pixel d’une image 800×400. Déterminer la largeur et la hauteur de cette image sachant qu’elle présente une résolution de 72 ppp.**

![item](images/trooperLogo.png) **Calculer la résolution d’une image bitmap de côté 10cm et de définition 800×800.**

## ![titre](images/titre.png) **Codage des couleurs**

Il  existe  plusieurs  modes  de  codage  informatique  des  couleurs,  le  plus  utilisé  pour  le  maniement  des images  est  l'espace colorimétrique  Rouge, Vert, Bleu  (RVB ou RVG : Red  Green  Blue)  par synthèse additive. 
Une image RVB est composée de la somme des trois rayonnements lumineux Rouge, Vert, Bleu dont les faisceaux sont superposés. A l'intensité maximale ils produisent une lumière blanche. La gamme des couleurs reproductibles par ce mode, quoique conditionnée par la qualité du matériel employé, est très étendue, et reproduit bien les couleurs saturées. En contrepartie, elle convient mal à la restitution des nuances délicates des lumières intenses et des tons pastel.

![melangeCouleur](images/melangeCouleurs.png)

Le codage de la couleur est réalisé sur3 octets dont les valeurs codent la couleur dans l'espace RVB. Chaque octet représente la valeur d'une composante couleur par un entier de 0 à 255. Le nombre de couleurs différentes est de $256\times 256\times 256 =16,8$ Millions. Une image numérique RVBest représentée par 3 tableaux à 2 dimensions dont la taille dépend du nombre de pixels contenus dans l’image. 

> **Panneau de modification des couleurs du logiciel Paint.**

![paint](images/paint.png)

Dans ce type d’image seul le niveau de l'intensité est codé sur un octet (256 valeurs). Par convention, la valeur 0 représente le noir (intensité lumineuse nulle) et la valeur 255 le blanc (intensité lumineuse maximale).

## ![titre](images/titre.png) **Conversion en niveaux de gris**

Il faut tenir compte de la physiologie de l'œil humain. En effet l'œil ne voit pas les trois composantes avec la même intensité. En effet, l'œil perçoit mieux les couleurs vertes que les couleurs bleues. Le rouge se situe entre les deux. Pour améliorer la conversion en niveau de gris on utilise le calcul de la luminance. Qui se fait comme suit :

$w_r\times R + w_g\times G + w_b\times B$

Où $w_r$, $w_g$ et $w_b$ sont les trois facteurs de pondération pour les trois couleurs R, G et B. Il existe beaucoup de normes différentes pour ces facteurs. Je vous en donne deux exemples. 

> **Pour le signal analogique de télévision : $w_r=0,299$, $w_g=0,587$ et $w_b=0,114$.**
>
> **Pour l'encodage digital de couleur : $w_r=0,2125$, $w_g=0,7154$ et $w_b=0,072$.**

**Encodage digital en niveaux de gris. On peut constater des niveaux de gris d’un rendu exceptionnel !**

![niveauxGris](images/niveauGris.png)

 ![item](images/trooperLogo.png) **Copier le code ci-dessous dans le fichier niveauxDeGris.py dans l'IDE Thonny et compléter la ligne 12 pour obtenir la conversion en niveau de gris précédente.**
 


```python
from PIL import Image
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

def niveauGris(addresseImage):
    photo = Image.open(addresseImage)
    pixels = np.array(photo)
    for i in range(pixels.shape[0]):
        for j in range(pixels.shape[1]):
            r, v, b = pixels[i, j]
            nuanceGris = __________________________
            pixels[i, j] = (nuanceGris, nuanceGris, nuanceGris)
    mpimg.imsave("gris.png", pixels)
```

## ![titre](images/titre.png) **Les différents systèmes de colorimétrie**

|Mode|Nombre de Bits par pixels|Nombre de couleurs|Remarques|
|:---:|:---:|:---:|:---:|
|Monochrome ou Noir et Blanc|1|2|Système utilisé pour scanner les textes pour faire de la reconnaissance de texte (OCR)|
|Niveaux de gris 8 256 Nuance de gris Mode 4 bits ou 16 couleurs|4|16|Palette de couleurs peu étendue réservée aux dessins simples sans couleurs nuancée|
|Mode 8 bits ou 256 couleurs|8|256|Palette de 256 couleurs qui permet de conserver une taille raisonnable|
|Mode 16 bits|16|65536|Palette de 64536 couleurs qui convient pour la plupart des usages|
|Mode 24 bits ou Couleurs RVB|24|16,7 millions|Mode utilisé par défaut par de nombreux logiciels|
|Couleurs CMJN|32|4,3 milliards|4 couleurs primaires : Cyan, Magenta, Jaune et Noir(256 teintes). Utilisé par les imprimantes|

![item](images/trooperLogo.png) **Indiquer par combien de bits est codée chacune des 3 couleurs en mode couleurs 24 bits (oucouleurs vraies). Donner la valeur minimale et maximale de chacune des 3 composantes.**
 
![item](images/trooperLogo.png) **Déterminer le nombre de nuances de couleurs obtenues avec ce type de codage couleur.**
  
![item](images/trooperLogo.png) **Indiquer quelle couleur est obtenue pour une intensité maximale des 3 couleurs RVB.**

![item](images/trooperLogo.png) **Indiquer quelle couleur est obtenue pour une intensité minimale des 3 couleurs RVB.**

![item](images/trooperLogo.png) **Lancer le logiciel PAINT(ou GIMP), puis ouvrir l’image soldat.bmp.**

![item](images/trooperLogo.png) **A l’aide du menu Image/Attributs, compléter la première ligne du tableau n°1.**

![item](images/trooperLogo.png) **Enregistrer l’image sous le nom soldat256.bmpavec le type Bitmap 256 couleurs.**

![item](images/trooperLogo.png) **Compléter la deuxième ligne du tableau n°1.**

![item](images/trooperLogo.png) **Ouvrir à nouveau l’image soldat.bmp. Enregistrer-la sous le nom soldat16.bmp avec le typeBitmap 16couleurs.**

![item](images/trooperLogo.png) **Compléter la troisième ligne du tableau n°1.**

![item](images/trooperLogo.png) **Ouvrir à nouveau l’image soldat.bmp. Enregistrez-la sous le nom soldat2.bmp avec le typeBitmapMonochrome.**

![item](images/trooperLogo.png) **Complétez la dernière ligne tableau n°1.**

![item](images/trooperLogo.png) **Justifier le lien entre les valeurs obtenues pour la dernière colonne et le nombre de bits par pixels.**


## **Tableau 1**

|Image|Nombre de pixel|Nombre maximum de couleurs|Taille fichier en ko|Nombre de bits par pixel|Qualité perçue de l'image|Rapport taille fichier / taille minimum*|
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|trooper24bits.bmp| | | | | | |
|trooper256.bmp| | | | | | |
|trooper16.bmp| | | | | | |
|trooper2.bmp| | | | | | |


![trooper](images/trooper.jpg)

## ![titre](images/titre.png) **Les différents formats d’images**

|Extension|Nombre de couleurs|Présentation|
|:--:|:--:|:--:|
|BMP|16,7 millions|Standard Windows. Format très répandu mais entrainant des fichiers très volumineux (très faible compression).|
|GIF|256|Standard Internet (excellente compression). Possibilité d’affichage progressif, animation ou de zone réactive.|
|PNG|256 à 16,7 millions|Même type que le format GIF mais avec des capacités de couleurs supérieures. Format ouvert.|
|PCX|16,7 millions|Format standard (Paintbrush) mais entrainant des fichiers très volumineux.|
|TIF|16,7 millions|Format reconnu par l’ensemble des machines, très utilisé en PAO. Image de qualité mais fichiers très volumineux.|
|JPEG (JPG)|16,7 millions|Standard internet et photo. Excellente compression avec plus ou moins de perte. Possibilité de zone réactive. Choix du taux de compression (donc de la qualité de l’image).|


![item](images/trooperLogo.png) **Ouvrir l’image city.bmp.**

![item](images/trooperLogo.png) **A l’aide du menu Image/Attributs, compléter la première ligne du tableau n°2.**

![item](images/trooperLogo.png) **Enregistrer l’image sous le nom cityGIF.gif avec le type GIF.**

![item](images/trooperLogo.png) **Compléter la deuxième ligne du tableau n°2.**

![item](images/trooperLogo.png) **Ouvrir à nouveau l’image city.bmp. Enregistrer-la sous cityJPG.jpg avec le type JPEG.**

![item](images/trooperLogo.png) **Complétez la troisième ligne du tableau n°2.**

![item](images/trooperLogo.png) **Justifier pourquoi la qualité de l’image au format GIF est-elle altérée.**

![item](images/trooperLogo.png) **Justifier s'il y a une différence de qualité entre l'image JPEG et l'original au format BMP (utiliser éventuellement la fonction ZOOM).**

![item](images/trooperLogo.png) **Déterminer le temps nécessaire pour la transmission de chacune des images précédentes sur une ligne ADSL ayant un débit idéal de 512 ko/s.**

![item](images/trooperLogo.png) **Justifier pourquoi le format JPG est un des formats les plus utilisés.**


## **Tableau 2**

|Image|Nombre de pixel|Taille du fichier en ko|Qualité perçue de l’image|
|:--:|:--:|:--:|:--:|
|city.bmp| | | |
|cityGIF.bmp| | | |
|cityJPG.bmp| | | |


![illustration](images/illustration.jpg)
